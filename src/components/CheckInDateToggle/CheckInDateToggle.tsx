import { ChangeEvent } from "react"
import Toggle from 'react-toggle'
import './style.css'

interface Props {
    isChecked: boolean;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

export const CheckInDateToggle = (props: Props) => {
    const { isChecked, onChange } = props;

    return (
        <>
            <Toggle
                name="checkInDateToggle"
                role="sortToggle"
                icons={false}
                checked={isChecked}
                onChange={onChange} />
            <span>Check In Date - closest → farthest</span>
        </>
    );
}