import { useState } from "react";
import { Style, IntDictionary } from "../../types/types";
import "./style.css";
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';

interface Props {
    styles: IntDictionary<number>;
    filterStyle: Style;
    onClick: (value: Style) => void;
}

export const UnitStyleFilter = (props: Props) => {
    const { styles, filterStyle, onClick } = props;
    const [dropdownOpen, setDropdownOpen] = useState(false)

    const toggle = () => setDropdownOpen(prevState => !prevState);

    return (
        styles &&
        (
            <Dropdown
                isOpen={dropdownOpen}
                toggle={toggle}
                value={filterStyle}
                role="styleFilter"
            >
                <DropdownToggle
                    className="dropDownToggle"
                    caret>
                    {filterStyle}
                </DropdownToggle>
                <DropdownMenu className='dropDownMenu'>
                    {Object.values(styles).map((style: Style, key) => (
                        <DropdownItem
                            className="dropDownItem"
                            key={key}
                            onClick={e => onClick(style)}
                            value={style}>
                            {style}
                        </DropdownItem>
                    ))
                    }
                </DropdownMenu>
            </Dropdown>
        )
    )
}