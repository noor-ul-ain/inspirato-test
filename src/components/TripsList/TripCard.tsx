import { Style } from "../../types/types";
import { fetchImageURL } from "../../service/ImgService/imgService";
import { Card } from 'reactstrap'
import './style.css'

interface Props {
    heroImage: string;
    unitName: string;
    unitStyleName: Style;
    checkInDate: string;
}

export const TripCard = (props: Props) => {
    const { heroImage, unitName, unitStyleName, checkInDate } = props;

    return (
        <Card className="tripCard" role="tripCard">
            <h3>{unitName} </h3>
            <img src={fetchImageURL(heroImage)} alt={unitName} />
            <p>{unitStyleName}
                <b> @ {new Date(checkInDate).toDateString()}</b></p>
        </Card>
    );
}