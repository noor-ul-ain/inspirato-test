import { TripSet } from "../../types/types";
import { TripCard } from "./TripCard";

import "./style.css";

interface Props {
    trips?: TripSet[];
}

export const TripsList = (props: Props) => {
    const { trips } = props

    return (
        <div className="tripsList">
            {trips &&
                trips.map((tripSet: TripSet, index) => (
                    <TripCard key={index} {...tripSet} />
                ))}
        </div>
    );
}


