import React, { useEffect, useState, ChangeEvent } from "react";
import { Style, Trips } from "../types/types";
import "./style.css";
import {
  fetchTripsData,
  sortByCheckInDate,
  filterByUnitStyle
} from "../service/TripService/tripService";

import { CheckInDateToggle } from "../components/CheckInDateToggle/CheckInDateToggle";
import { TripsList } from "../components/TripsList/TripsList";
import { UnitStyleFilter } from "../components/UnitStyleFilter/UnitStyleFilter"

interface Props {
  mockData?: Trips;
}

export const App = (props: Props) => {
  const { mockData } = props;
  const [checked, setChecked] = useState(true)
  const [filterStyle, setFilterStyle] = useState<Style>(Style["All Vacations"]);
  const [tripsData, setTripsData] = useState<Trips | undefined>(mockData);

  useEffect(() => {
    setTripsData(fetchTripsData())
  }, []);

  const toggleCheckInDate = (e: ChangeEvent<HTMLInputElement>) => {
    setChecked(prevState => !prevState)
    setTripsData({
      ...tripsData!,
      tripSet: sortByCheckInDate(tripsData!.tripSet, !checked)
    });
  }

  const filterByStyle = (style: Style) => {
    setFilterStyle(style);
  }

  return (
    <div className="mainBodyContainer">
      <div className="mainContainerTop">
        <h1>INSPIRATO</h1>
        <div className="filters">
          <CheckInDateToggle isChecked={checked} onChange={toggleCheckInDate} />
          <UnitStyleFilter
            styles={tripsData?.styles || {}}
            onClick={filterByStyle}
            filterStyle={filterStyle}
          />
        </div>
      </div>
        <TripsList
          trips={filterStyle === Style["All Vacations"]
            ? tripsData?.tripSet
            : filterByUnitStyle(tripsData?.tripSet || [], filterStyle)
          }
        />
    </div>
  );
}

export default App;
