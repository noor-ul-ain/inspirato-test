const BASE_URL = "https://cms.inspirato.com";
const IMAGE_SERVICE = "ImageGen.ashx";
const WIDTH = "450";
const HEIGHT= "350"

export const fetchImageURL=(path: string)=> {
  const urlParams = new URLSearchParams({
    image: path,
    width: WIDTH,
    height:HEIGHT
  });

  return `${BASE_URL}/${IMAGE_SERVICE}?${urlParams.toString()}`;
}
