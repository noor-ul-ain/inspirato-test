import { fetchImageURL } from "./imgService";

describe("ImgService", () => {
  describe("fetchImageURL", () => {
    it("should create a URL for images", () => {
      const fetchImgURL = fetchImageURL("/test.img");
      expect(fetchImgURL).toEqual(
        "https://cms.inspirato.com/ImageGen.ashx?image=%2Ftest.img&width=450&height=350"
      );
    });
  });
});