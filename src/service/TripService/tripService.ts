
import { Style, TripSet } from '../../types/types'
import data from "../../trips.json"

export const fetchTripsData = () => {
 const results=JSON.parse(JSON.stringify(data))

 return {...results, tripSet: sortByCheckInDate(results.tripSet,true)}
}

export const filterByUnitStyle = (trips:TripSet[] , filter:Style)=>
  trips.filter((tripSet: TripSet) => tripSet.unitStyleName === filter)

export const sortByCheckInDate = (trips: TripSet[], latest: boolean) => {
  return trips.sort((x: TripSet, y: TripSet) => {
    const prevDate = new Date(y.checkInDate).valueOf();
    const nextDate = new Date(x.checkInDate).valueOf();

    return latest ? prevDate - nextDate : nextDate - prevDate;
  });
}