import { Style } from "../../types/types"
import {
  sortByCheckInDate,
  filterByUnitStyle,
} from "./tripService";

const testData= [
  {
    heroImage: "",
    unitName: "",
    unitStyleName: Style["All Vacations"],
    checkInDate: "2022-04-02T00:00:00Z",
  },
  {
    heroImage: "",
    unitName: "",
    unitStyleName: Style["Lifestyle"],
    checkInDate: "2022-08-11T00:00:00Z",
  },
  {
    heroImage: "",
    unitName: "",
    unitStyleName: Style["Beach"],
    checkInDate: "2021-09-16T00:00:00Z",
  },
  {
    heroImage: "",
    unitName: "",
    unitStyleName: Style["Mountain"],
    checkInDate: "2021-02-16T00:00:00Z",
  },
  {
    heroImage: "",
    unitName: "",
    unitStyleName: Style["Metropolitan"],
    checkInDate: "2021-06-16T00:00:00Z",
  }
];

describe("tripService", () => {
  describe("sortByCheckInDate", () => {
    it("should return sorted trips based on CheckInDate closest → farthest", () => {
      expect(sortByCheckInDate(testData, true)).toEqual([
        {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Lifestyle"],
            checkInDate: "2022-08-11T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["All Vacations"],
            checkInDate: "2022-04-02T00:00:00Z",
          },
       
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Beach"],
            checkInDate: "2021-09-16T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Metropolitan"],
            checkInDate: "2021-06-16T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Mountain"],
            checkInDate: "2021-02-16T00:00:00Z",
          }    
      ]);
    });

    it("should return sorted trips based on CheckInDate farthest → closest",  () => {
      expect(sortByCheckInDate(testData, false)).toEqual([
        {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Mountain"],
            checkInDate: "2021-02-16T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Metropolitan"],
            checkInDate: "2021-06-16T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Beach"],
            checkInDate: "2021-09-16T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["All Vacations"],
            checkInDate: "2022-04-02T00:00:00Z",
          },
          {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Lifestyle"],
            checkInDate: "2022-08-11T00:00:00Z",
          }
      ]);
    });
  });

    it("should return based on unitStyle",  () => {
      expect(filterByUnitStyle(testData, Style["Mountain"])).toEqual([
        {
            heroImage: "",
            unitName: "",
            unitStyleName: Style["Mountain"],
            checkInDate: "2021-02-16T00:00:00Z",
        },
      ]);
    });
  });
