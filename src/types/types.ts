export enum Style {
  "All Vacations" = "All Vacations",
  "Beach" = "Beach",
  "Lifestyle" = "Lifestyle",
  "Metropolitan" = "Metropolitan",
  "Mountain" = "Mountain",
}

export interface TripSet {
  heroImage: string;
  unitName: string;
  unitStyleName: Style;
  checkInDate: string;
  //...
}
export interface Dictionary<T> {
    [Key: string]: T;
  }
  
  export interface IntDictionary<T> {
    [Key: number]: T;
  }
  
  export interface Trips{
    tripSet: TripSet[];
    dates: Dictionary<string>;
    description: string;
    destinations: string[];
    name: string;
    id: number;
    showTripValue: boolean;
    styles: IntDictionary<number>;
  }